# Pre-Requirements

Brief description of project.

## Installation

1. **Clone the Repository:**

   ```
   git clone https://gitlab.com/ishkhan.grigoryan7/pre-requirements.git
   cd pre-requirements
    ```
   
2. **Create a virtual environment and activate one**

    ```bash
    python -m venv venv
    ```
   On Windows
    ```
   .\venv\Scripts\activate
   ```
   
   On macOS/Linux:
   ```bash
   source venv/bin/activate
   ```
3. **Install dependencies**
   ```bash
   pip install -r requirements.txt
   ```
4. **Run the application**
    ```bash
    python3 main.py 1 3
    python3 main.py 1 1
    python3 main.py 10 3
    ```

