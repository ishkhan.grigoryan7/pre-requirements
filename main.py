import requests
import json
import sys

BASE_URL = 'https://webqa.mercdev.com/'


def read_config(file_path="config.json"):
    """
    Чтение данных конфигурации из файла JSON.

    :param file_path: Путь к файлу конфигурации.
    :return: Словарь данных конфигурации.
    """
    try:
        with open(file_path, "r") as config_file:
            config_data = json.load(config_file)
        return config_data
    except FileNotFoundError:
        print(f"Ошибка: Файл конфигурации '{file_path}' не найден.")
        sys.exit(1)
    except json.JSONDecodeError:
        print(f"Ошибка: Невозможно разобрать JSON в файле '{file_path}'. Проверьте формат файла.")
        sys.exit(1)


# Аутентификация пользователя
def authenticate(email, password):
    """
    Аутентификация пользователя.

    :param email: Адрес электронной почты пользователя.
    :param password: Пароль пользователя.
    :return: Токен аутентификации.
    """
    url = BASE_URL + "api/v1/user/login"
    headers = {
        'Content-Type': 'application/json'
    }

    payload = {
        "email": email,
        "password": password
    }

    response = requests.post(url, json=payload, headers=headers)
    auth_token = response.json().get('authToken')
    return auth_token


# Получение списка товаров
def get_product_list(auth_token):
    """
    Получение списка товаров.

    :param auth_token: Токен аутентификации.
    :return: Словарь, сопоставляющий идентификатор товара и его цену.
    """
    url = BASE_URL + "api/v1/product/"
    headers = {
        'Authorization': auth_token
    }

    response = requests.get(url, headers=headers)
    product_list = response.json()
    price_map = {product['id']: product['price'] for product in product_list.get('products', [])}
    print(price_map)
    return price_map


# Покупка товара
def make_purchase(auth_token, config_data, product_id, product_quantity):
    """
    Покупка товара.

    :param auth_token: Токен аутентификации.
    :param config_data: Данные конфигурации, включая информацию о карте.
    :param product_id: Идентификатор товара.
    :param product_quantity: Количество товара.
    :return: Ответ сервера в формате JSON.
    """
    url = BASE_URL + "api/v1/order/createAndPay"

    headers = {
        'Authorization': auth_token,
        'Content-Type': 'application/json'
    }

    payload = {
        "card": {
            "number": config_data["card_number"],
            "date": config_data["card_date"],
            "name": config_data["card_name"],
            "cvv": config_data["card_csv"]
        },
        "products": [
            {
                "id": product_id,
                "quantity": product_quantity
            }
        ]
    }

    response = requests.post(url, json=payload, headers=headers)
    return response.json()


# Проверка аргументов командной строки
if len(sys.argv) != 3:
    print("Ошибка: Недостаточно аргументов командной строки для идентификации товара и его количества!")
    sys.exit(1)

product_id = sys.argv[1]

try:
    product_quantity = int(sys.argv[2])
except ValueError:
    print("Ошибка: Количество товара должно быть целым числом.")
    sys.exit(1)

# Основная часть программы
if __name__ == "__main__":
    config_data = read_config("config.json")
    auth_token = authenticate(config_data["email"], config_data["password"])
    price_map = get_product_list(auth_token)

    if product_id not in price_map:
        print(f"Ошибка: Товар с идентификатором {product_id} не найден.")
        sys.exit(1)

    product_price = price_map[product_id]
    expected_total_sum = product_price * product_quantity
    if product_quantity >= 3:
        expected_total_sum -= 0.1 * expected_total_sum

    response_data = make_purchase(auth_token, config_data, product_id, product_quantity)

    if 'message' not in response_data:
        print("Ошибка: 'message' ключ не найден в ответе сервера.")
        sys.exit(1)

    print("Сообщение от сервера:", response_data['message'])

    if 'transaction' not in response_data:
        print("Ошибка: 'transaction' ключ не найден в ответе сервера.")
        sys.exit(1)

    if 'order' not in response_data['transaction']:
        print("Ошибка: 'order' ключ не найден в ключе 'transaction'.")
        sys.exit(1)

    actual_total_sum = response_data['transaction']['order'].get('totalSum')

    if actual_total_sum is not None and actual_total_sum == expected_total_sum:
        print("Ok, Сумма совпадает с ожидаемым значением.")
    else:
        print("Ошибка: Сумма не совпадает с ожидаемым значением.")
